# RustEez

A framework to create RLBot bots. These bots have two parts. The strategical part, and the driving part.
They are respectively referred to as the copilot and the pilot. There can actually be several copilots working together, same with the pilots.
A communication based approach to making decisions is used, allowing for separating tasks and multithreading.


## Quick overview

There are four main entities to create or customize:

- **Copilot**: decides what to do
- **Pilot**: does it
- **Messages**: the way they both communicate
- **Protocol**: the way they communicate with external bots, like through Quick chat for example

The Copilot(s) and the Pilot(s) (referred to as the **Cockpit**) run within a Context.

The **Context** manages the communication between Pilots, Copilots, other bots and the world they are in (the data packets).
If you want to know what's going on it's the thing you're looking for!

