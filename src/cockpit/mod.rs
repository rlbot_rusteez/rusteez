pub mod copilot;
pub mod pilot;

use crate::cockpit::pilot::Pilot;
use crate::cockpit::copilot::Copilot;

use std::sync::mpsc::{self, Sender, Receiver};
use crate::context::Radio;
use std::fmt::Debug;

pub trait Racer<Msg, Ptc>
    where Msg: Clone + PartialEq + Debug,
        Ptc: Clone + PartialEq + Debug {
    fn start(&mut self);
    fn name(&self) -> String;
    fn world_source(&mut self, source: Receiver<Radio<Msg, Ptc>/*[feat] use Radio, to manage the team communications (hive and chat messages)*/>);
}

pub trait Mate<R> {
    fn add_pilot_mate(&mut self, pilot: &mut impl Mate<R>);
    fn add_copilot_mate(&mut self, copilot: &mut impl Mate<R>);

    fn add_pilot_source(&mut self, pilot: Receiver<R>);
    fn add_copilot_source(&mut self, copilot: Receiver<R>);
}