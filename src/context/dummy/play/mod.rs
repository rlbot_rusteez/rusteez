
use crate::context::Context;
use crate::Bot;
use std::fmt::Debug;

pub struct Play {

}

impl<Msg, Ptc> Context<Msg, Ptc> for Play
    where Msg: Clone + PartialEq + Debug,
          Ptc: Clone + PartialEq + Debug {
    fn run(&mut self, bot: Vec<Bot<Msg, Ptc>>) -> Result<(), ()> {
        println!("Oioi!");
        Ok(())
    }
}