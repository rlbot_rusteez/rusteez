use rlbot::{RLBot, Packeteer, GameTickPacket, init_with_options, InitOptions, RenderGroup, ControllerState};
use crate::context::{Context, Radio, RacerState, Contexts, RacerShow};
use crate::{Bot, context};
use std::env::Args;

//use crate::{todo_warn, todo_error};
use std::sync::Arc;
use log::{trace, debug, warn, error};
use crate::say;
use std::fs::File;
use std::io::Read;
use std::str::FromStr;
use toml;
use toml::de::Error;
use serde::Deserialize;
use std::error;
use crate::shout;
//use crate::misc::find_rlbot_dir;
use rlbot::parse_hive_framework_args;
use std::fmt::Debug;
#[cfg(feature = "performance-check")]
use std::time::Instant;


pub struct Play {
    rlbot: &'static RLBot,
    previous_packet: Option<Arc<GameTickPacket>>
}

impl Play {
    pub fn new_with_rlbot(rlbot: &'static RLBot, args: Args) -> (Play, Args) {
        (Play {
            rlbot,
            previous_packet: None,
        }, args)
    }

    pub fn try_new(args: Args) -> Result<(Play, Args), Box<dyn error::Error>> {

        let rlbot = rlbot::init()?;
        //_with_options(InitOptions::default().rlbot_dll_directory(find_rlbot_dir().expect("Couldn't find rlbot's directory")))?;
        let rlbot: &rlbot::RLBot = Box::leak(Box::new(rlbot));
        Ok(Self::new_with_rlbot(rlbot, args))
    }

    pub fn try_new_from_args(args: Args) -> Result<(Result<(Contexts, Vec<usize>), ()>, Args), String> {
        trace!("trying to find a framework");
        #[cfg(not(feature = "hive"))]
            {
                trace!("trying to build a bot");
                use rlbot::parse_framework_args;
                match parse_framework_args()
                    .map_err(|_| String::from("could not parse framework arguments"))? {
                    Some(framework_args) => {
                        let indexes = vec![framework_args.player_index as usize];
                        let rlbot = init_with_options(framework_args.into())
                            .map_err(|_| String::from("An RLBot instance already exists!"))?;
                        let rlbot: &rlbot::RLBot = Box::leak(Box::new(rlbot));

                        let (context, args) = Self::new_with_rlbot(rlbot, args);
                        Ok((Ok((Contexts::RLBotPlay(context), indexes)), args))
                    },
                    None => {
                        Ok((Err(()), args))
                    }
                }
            }

        #[cfg(feature = "hive")]
            {
                trace!("trying to build a hive mind");
                use rlbot::parse_hive_framework_args;
                match parse_hive_framework_args()
                    .map_err(|_| String::from("could not parse framework arguments"))? {
                    Some(framework_args) => {
                        let indexes: Vec<usize> = framework_args.drone_indices.iter().map(|idx|{*idx as usize}).collect();
                        let rlbot = init_with_options(From::from(&framework_args))
                            .map_err(|_| String::from("An RLBot instance already exists!"))?;
                        let rlbot: &rlbot::RLBot = Box::leak(Box::new(rlbot));

                        let (context, args) = Self::new_with_rlbot(rlbot, args);
                        Ok((Ok((Contexts::RLBotPlay(context), indexes)), args))
                    },
                    None => {
                        Ok((Err(()), args))
                    }
                }
            }

    }

    pub fn start_match(&mut self, settings: &rlbot::MatchSettings) -> Result<(), Box<dyn error::Error>> {
        self.rlbot.start_match(settings)?;
        self.rlbot.wait_for_match_start()?;
        Ok(())
    }

    /*
    pub fn try_new_from_config_file(filepath: &str, args: Args) -> Result<(Play, Args), ParseConfigError> {
        Ok(Self::new(filepath.parse::<Config>()?, args))
    }
     */

    fn tick<Msg: PartialEq + Clone + Debug, Ptc: PartialEq + Clone + Debug> (&mut self, packeteer: &mut Packeteer, bots: &mut Vec<Bot<Msg, Ptc>>) {
        #[cfg(feature = "performance-check")]
        let total_turn = Instant::now();
        #[cfg(feature = "performance-check")]
        let total = Instant::now();

        let mut packet = loop {#[cfg(feature = "performance-check")] let now = Instant::now(); if let Ok(packet) = packeteer.next_flatbuffer() {#[cfg(feature = "performance-check")] debug!("simple fetch: {}", now.elapsed().as_millis());break packet;}; debug!("Ten seconds have passed !")};
        let mut count = 0; loop {if let Some(p) = packeteer.try_next_flat() {packet = p;count += 1;} else {break}} if count > 0 {debug!("dropped {} packets", count)};
        let arc_packet = Arc::new(packet);

        #[cfg(feature = "performance-check")]
        debug!("== total fetch: {}", total.elapsed().as_millis());



        // Determine state
        let state = if
            *&self.previous_packet.as_ref().and_then(|packet|{Some(&arc_packet.game_info.game_time_remaining == &packet.game_info.game_time_remaining)})
                .or_else(||{Some(false /*first packet, thus not the same as the previous one*/)})
                .unwrap(/*They're both Some*/)
        {
            // if replay
            if arc_packet.ball.as_ref().unwrap().physics.location.x != 0. && arc_packet.ball.as_ref().unwrap().physics.location.y != 0. {
                trace!("Paused");
                RacerState::Pause
            }
            else if !&arc_packet.game_info.is_kickoff_pause {
                trace!("Pondering!");
                RacerState::Ponder(Arc::clone(&arc_packet))
            }
            else {
                trace!("Kickoff!");
                RacerState::Play(Arc::clone(&arc_packet))
            }
        }
        else if arc_packet.game_info.is_match_ended {
                trace!("Stop!");
            RacerState::Stop
        }
        else {
            RacerState::Play(Arc::clone(&arc_packet))
        };
        self.previous_packet = Some(arc_packet.clone());

        let state = Arc::new(state);

        for bot in bots {

            #[cfg(feature = "performance-check")]
            {
                let mut group = self.rlbot.begin_render_group(bot.index as i32);
                let green = group.color_rgb(0, 255, 0);
                let red = group.color_rgb(255, 0, 0);
                group.draw_line_3d((arc_packet.ball.as_ref().unwrap().physics.location.x, arc_packet.ball.as_ref().unwrap().physics.location.y, 50.), (arc_packet.ball.as_ref().unwrap().physics.location.x, arc_packet.ball.as_ref().unwrap().physics.location.y, 500.), green);
                group.draw_line_3d((-893f32, -5120f32, 50.), (-893f32, 5120f32, 50.), red);
                group.draw_line_3d((893f32, -5120f32, 50.), (893f32, 5120f32, 50.), red);
                group.render().unwrap();
            }

            /*=== Simple controller state input ===
            let side = if (arc_packet.game_info.seconds_elapsed as u32 / 10 ) % 2 == 0 {-1} else {1};
            let cs = ControllerState {
                throttle: 1.0,
                steer: (1 * side) as f32,
                pitch: 0.0,
                yaw: 0.0,
                roll: 0.0,
                jump: false,
                boost: false,
                handbrake: false,
                use_item: false
            };
            self.rlbot.update_player_input(bot.index as i32, &cs).unwrap(); //[maybe] use rlbot::update_multiple_inputs()
            ===*/


            self.get_radio_data(bot, &state, None);


            #[cfg(feature = "performance-check")]
            debug!("== TOTAL TURN: {}", total_turn.elapsed().as_millis());
        }
    }


    //[behaviour] The *last opened* rendering context is exited with an exit message or a decision (that can be empty).
    fn get_radio_data<Msg: PartialEq + Clone + Debug, Ptc: PartialEq + Clone + Debug> (&mut self, bot: &mut Bot<Msg, Ptc>, state: &Arc<RacerState>, mut group: Option<RenderGroup>) -> bool/*exit_all*/ {
        loop {
            match self.bot_exchange(bot, &state) {

                // Decision received. Exiting everything
                Some(Radio::Decision(input, index)) => {
                    debug!("input: {:?}", input);
                    self.rlbot.update_player_input(index as i32, input.controller_state()).unwrap(); //[maybe] use rlbot::update_multiple_inputs()
                    break true;
                },

                // Empty decision. Exiting everything
                None => break true,



                // Rendering data received. Continuing
                Some(Radio::Show(RacerShow::Render3DLine(start, end, color), index)) => {
                    if let Some(group) = &mut group {
                        let color = group.color_rgb(color.x, color.y, color.z);
                        group.draw_line_3d((start.x, start.y, start.z), (end.x, end.y, end.z), color);
                    }
                    else {
                        error!("Tried to draw without having created a render group.");
                    }
                },

                // Rendering start. Entering
                Some(Radio::Show(RacerShow::Render3DLineBegin, index)) => {
                    let mut group = self.rlbot.begin_render_group(index as i32);
                    if self.get_radio_data(bot, state, Some(group)) {
                        break true;
                    }
                },

                // Rendering stop. Exiting
                Some(Radio::Show(RacerShow::Render3DLineEnd, index)) => {
                    if let Some(group) = group {
                        group.render().unwrap();
                        break false;
                    }
                    else {
                        error!("Tried to close a group without having created one previously.");
                    }
                },

                // Something else. Continue
                a @ _ => warn!("Unmanaged bot return radio data: {:?}", a),
            }
        }
    }

    fn bot_exchange<Msg: PartialEq + Clone + Debug, Ptc: PartialEq + Clone + Debug> (&mut self, bot : &mut Bot<Msg, Ptc>, state: &Arc<RacerState>) -> Option<Radio<Msg, Ptc>> {
        for racer in &bot.racers {
            racer.send(Radio::Context(state.clone(), bot.index)).unwrap();
        }
        if let RacerState::Play(_) = **state {
            bot.decider.try_recv().ok() // Avoids waiting for the bot's decision
        }
        else {None}
    }
}

impl<Msg, Ptc> Context<Msg, Ptc> for Play
    where Msg: Clone + PartialEq + Debug,
        Ptc: Clone + PartialEq + Debug {
    fn run(&mut self, mut bot: Vec<Bot<Msg, Ptc>>) -> Result<(), ()> {

        let mut packeteer = self.rlbot.packeteer();
        let packet_game_start = packeteer.next().unwrap();

        warn!("Set up the context with the first packet");
        loop {
            self.tick(&mut packeteer, &mut bot);
        }
    }
}
/*
#[derive(Deserialize)]
pub struct Config<'a> {
    #[serde(default = default_player_configurations)]
    pub player_configurations: Vec<PlayerConfiguration<'a>>,
    #[serde(default = default_game_mode)]
    pub game_mode: GameMode,
    #[serde(default = default_game_mode)]
    pub game_map: GameMap,
    #[serde(default = default_skip_replays)]
    pub skip_replays: bool,
    #[serde(default = default_instant_start)]
    pub instant_start: bool,
    #[serde(default = default_mutator_settings)]
    pub mutator_settings: MutatorSettings,
}

impl FromStr for Config {
    type Err = ParseConfigError;

    fn from_str(string: &str) -> Result<Self, Self::Err> {
        Ok(toml::from_str::<Self>(string)?)
    }
}

pub enum ParseConfigError {
    TomlSyntaxError(toml::de::Error)
}

impl From<toml::de::Error> for ParseConfigError {
    fn from(error: Error) -> Self {
        ParseConfigError::TomlSyntaxError(error)
    }
}

fn default_game_mode() -> GameMode {
    GameMode::Soccer
}
fn
*/