
use crate::context;
use rlbot::{GameTickPacket, Packeteer, ControllerState};
use std::sync::Arc;
use log::info;
use std::env::Args;
use crate::Bot;
use log::trace;
use std::fmt::{self, Debug, Formatter};
use na::{Vector3};


// 1

pub trait Context<Msg, Ptc>
    where Msg: Clone + PartialEq + Debug,
          Ptc: Clone + PartialEq + Debug {
    fn run(&mut self, bots: Vec<Bot<Msg, Ptc>>) -> Result<(), ()>;
}

// #[derive(Context)]
pub enum Contexts {
    DummyPlay(context::dummy::Play),
    RLBotPlay(context::rlbot::Play),
    RLBotTrain(context::rlbot::Train),
}

impl<Msg, Ptc> Context<Msg, Ptc> for Contexts
    where Msg: Clone + PartialEq + Debug,
          Ptc: Clone + PartialEq + Debug {
    fn run(&mut self, bots: Vec<Bot<Msg, Ptc>>) -> Result<(), ()> {
        match self {
            Contexts::RLBotPlay(play) => {
                play.run(bots)
            }
            Contexts::RLBotTrain(train) => {
                train.run(bots)
            }
            Contexts::DummyPlay(dummy) => {
                dummy.run(bots)
            }
        }
    }
}

#[derive(Clone)]
pub enum RacerState {
    Play(Arc<GameTickPacket>),
    Ponder(Arc<GameTickPacket>),
    Pause,
    Stop,
    Ping(u8)
}

impl PartialEq for RacerState {
    fn eq(&self, other: &Self) -> bool {
        unimplemented!()
    }
}

impl Debug for RacerState {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            RacerState::Play(packet) => {
                let p = "Arc<GameTickPacket> (fields omitted)".to_string();
                f.debug_struct("RacerState::Play"/*"(Arc<GameTickPacket>)"*/)
                    //.field("boost_pad_states", &packet.as_ref().boost_pad_states)
                 .field("packet", &p)
                 .finish()
            },
            RacerState::Ponder(packet) => {
                let p = "Arc<GameTickPacket> (fields omitted)".to_string();
                f.debug_struct("RacerState::Ponder")
                 .field("packet", &p)
                 .finish()
            },
            RacerState::Pause => {
                f.debug_struct("RacerState::Pause")
                 .finish()
            },
            RacerState::Stop => {
                f.debug_struct("RacerState::Stop")
                 .finish()
            },
            RacerState::Ping(x) => {
                f.debug_struct("RacerState::Ping")
                 .field("x", x)
                 .finish()
            },
        }
    }
}


/*
impl std::cmp::PartialEq for RacerState {
    fn eq(&self, other: &Self) -> bool {
        match self {
            RacerState::Play(packet) => {
                if let RacerState::Play(packet2) = other {
                    /*[MAINTENANCE] comparing non_exhaustived structs*/
                    packet.as_ref().players == packet2.as_ref().players
                        && packet.as_ref().boost_pad_states == packet2.as_ref().boost_pad_states
                        && packet.as_ref().ball == packet2.as_ref().ball
                        && packet.as_ref().game_info == packet2.as_ref().game_info
                        && packet.as_ref().tile_information == packet2.as_ref().tile_information
                        && packet.as_ref().teams == packet2.as_ref().teams
                }
                else {false}
            },
            RacerState::Ponder(packet) => {
                    /*[MAINTENANCE] comparing non_exhaustived structs*/
                if let RacerState::Ponder(packet2) = other {
                    packet.as_ref().players == packet2.as_ref().players
                        && packet.as_ref().boost_pad_states == packet2.as_ref().boost_pad_states
                        && packet.as_ref().ball == packet2.as_ref().ball
                        && packet.as_ref().game_info == packet2.as_ref().game_info
                        && packet.as_ref().tile_information == packet2.as_ref().tile_information
                        && packet.as_ref().teams == packet2.as_ref().teams
                }
                else {false}
            },
            RacerState::Pause => {
                if let RacerState::Pause = other {true}
                else {false}
            },
            RacerState::Stop => {
                if let RacerState::Stop = other {true}
                else {false}
            },
            RacerState::Ping(x) => {
                if let RacerState::Ping(x2) = other {
                    x == x2
                }
                else {false}
            }
        }
    }
}*/

#[derive(Clone, PartialEq, Debug)]
pub enum RacerShout {
    Help,
    Warning(String),
    Panicking(String),
}

#[derive(Clone, PartialEq, Debug)]
pub enum RacerShow {
    Render3DLine(Vector3<f32> /*start*/, Vector3<f32> /*end*/, Vector3<u8> /*color*/),
    Render3DLineBegin,
    Render3DLineEnd,

}

#[derive(Clone, PartialEq, Debug)]
pub enum Radio<Msg, Ptc>
    where Msg: Clone + PartialEq + Debug,
        Ptc: Clone + PartialEq + Debug { // usize are the bot's index
    // Context <-> Racer
    Context(Arc<RacerState>, usize),
    Decision(Input, usize),
    Show(RacerShow, usize),
    Shout(RacerShout, usize),

    // Racer <-> Racer
    //Within Cockpit
    Racer(Msg, usize),
    //Within Team
    Team(Ptc, usize),
}

pub struct Input {
 controller: ControllerState
}

impl Clone for Input {
    fn clone(&self) -> Self {
        unimplemented!()
    }
}

impl  PartialEq for Input {
    fn eq(&self, other: &Self) -> bool {
        unimplemented!()
    }
}

impl Debug for Input {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let p = "ControllerState (fields omitted)".to_string();
        f.debug_struct("Input")
            .field("controller", &p)
            .finish()
    }
}


impl Input {
    pub fn new(controller: ControllerState) -> Self {
        Input { controller }
    }
    pub fn controller_state(&self) -> &ControllerState {
        &self.controller
    }
}
