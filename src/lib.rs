pub mod mission_control;
pub mod context;
pub mod cockpit;
pub mod world;

pub mod misc;

use crate::cockpit::{Mate, Racer};
use crate::context::{Radio, RacerState};

use std::sync::mpsc::{Sender, Receiver, channel};
use std::time::Duration;
use std::env::Args;
use std::{default, thread};
use rand::prelude::random;
use log::trace;
use rlbot::{parse_framework_args, init_with_options};
use std::fmt::Debug;


/// Builds a Bot, representing a car's cockpit.
///
/// # Examples
///
/// ```rust
/// use rusteez::BotBuilder;
///
/// // create an rlbot and retrieve the index. For example purposes, a dummy index is used
/// let rlbot_index = 0;
///
/// // Create a BotBuilder with the desired Message type (for the communication between pilots and copilots) and Protocols (for the communication between bots in the same team). We'll leave it out for the example
/// let mut bot_builder: BotBuilder<(), ()> = BotBuilder::new()
///     .name("MyBot")
///     .index(rlbot_index);
///
/// // Give the Sender of the decision channel to the Pilot that will send inputs to the framework
/// let decision_sender = bot_builder.new_decider_channel();
/// // With the Pilot of Onyx for example, this is done at the Pilot's creation: `let mut petra = onyx_lib::pilot::Petra::new(&mut bot_builder);` calls `new_decider_channel()` and saves the Sender.
///
/// let bot = bot_builder.build();
/// ```
#[derive(Default)]
pub struct BotBuilder<Msg, Ptc>
    where Msg: Clone + PartialEq + Debug,
        Ptc: Clone + PartialEq + Debug {
    name: Option<String>,
    index: Option<usize>,
    racers: Vec<Sender<Radio<Msg, Ptc>>>,
    decider: Option<Receiver<Radio<Msg, Ptc>>>
}

impl<Msg, Ptc> BotBuilder<Msg, Ptc>
    where Msg: Clone + PartialEq + Debug + Default,
        Ptc: Clone + PartialEq + Debug + Default {
    pub fn new() -> BotBuilder<Msg, Ptc> {
        BotBuilder::default()
    }

    /// Builds a new Bot
    ///
    /// # Panics
    ///
    /// This method panics when no index nor decider has been given, for now.
    pub fn build(self) -> Bot<Msg, Ptc> {
        let name = self.name.unwrap_or(String::from("bot_")+random::<u8>().to_string().as_str());

        Bot {
            name,
            index: self.index.expect("No index specified"),//[todo] soften the situation handling
            racers: self.racers,
            decider: self.decider.expect("No decider specified"),//[todo] soften the situation handling
        }

    }

    /// Sets the name of the bot
    pub fn name(mut self, name: &str) -> BotBuilder<Msg, Ptc> {
        self.name = Some(name.to_string());
        self
    }

    /// Sets the index of the bot
    pub fn index(mut self, index: usize) -> BotBuilder<Msg, Ptc> {
        self.index = Some(index);
        self
    }

    /// Adds a racer communication channel
    pub fn new_racer_channel(&mut self) -> Receiver<Radio<Msg, Ptc>> {
        let (sender, receiver) = channel();
        self.racers.push(sender);
        receiver
    }

    /// Adds a decider communication channel
    pub fn new_decider_channel(&mut self) -> Sender<Radio<Msg, Ptc>> {
        let (sender, receiver) = channel();
        self.decider = Some(receiver);
        sender
    }
}

/// Gateway to the bot's entities (pilot(s) and copilot(s)).
pub struct Bot<Msg, Ptc>
    where Msg: Clone + PartialEq + Debug,
        Ptc: Clone + PartialEq + Debug {

    /// The bot's name
    name: String,

    /// The bot's index, used for identification
    index: usize,

    /// Packet receivers
    racers: Vec<Sender<Radio<Msg, Ptc>>>,

    /// Receive the bot's Inputs
    decider: Receiver<Radio<Msg, Ptc>>,

}

impl<Msg, Ptc> Bot<Msg, Ptc>
    where Msg: Clone + PartialEq + Debug,
        Ptc: Clone + PartialEq + Debug {

    /// Returns the bot's name
    pub fn name(&self) -> String {
        self.name.clone()
    }

    /// Returns the bot's index
    pub fn index(&self) -> usize {
        self.index
    }

    /// Returns the bot's identifier, which is it's id for now.
    pub fn id(&self) -> usize {
        self.index
    }
}




#[cfg(test)]
mod tests {
    use crate::{BotBuilder, Bot};
    use crate::context::{Radio, RacerState, Input};
    use rlbot::ControllerState;


    /// Receive a bot decision
    #[test]
    fn new_bot_decision() {
        let rlbot_index = 0;

        let mut bot_builder: BotBuilder<usize, String> = BotBuilder::new()
            .name("Test Bot")
            .index(rlbot_index);

        let sender = bot_builder.new_decider_channel();

        let mut bot = bot_builder.build();

        sender.send(Radio::Decision(Input::new(ControllerState::default()), 0));
        /*assert_eq!(
            bot.decider.recv().unwrap(/*sent above*/),
            Radio::Decision(Input::new(ControllerState::default()), 0)
        );*/
    }

    /// New bot creation
    #[test]
    fn new_bot() {
        let rlbot_index = 0;

        let mut bot_builder: BotBuilder<usize, String> = BotBuilder::new()
            .name("Test Bot")
            .index(rlbot_index);

        let sender = bot_builder.new_decider_channel();

        let mut bot = bot_builder.build();

        assert_eq!(
            (
                bot.name,
                bot.index,
                bot.racers.len()
            ),
            (
                "Test Bot".to_string(),
                0,
                0
            )
        );
    }

}
