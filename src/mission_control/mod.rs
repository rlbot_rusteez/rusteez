pub mod project;
pub mod logging;
pub mod damage_control;

use log::LevelFilter;


/// Set up debugging tools.
///
/// Sets up the logging.
pub fn setup(level_filter: Option<LevelFilter>, lib_filters: Vec<(&'static str, log::LevelFilter)>) {
    logging::setup(level_filter, lib_filters);
}