use std::thread;
use log::Level;
use palette::{self, Hue, Saturate, LinSrgb, Hsv};
use std::process;


/// Sets up the logging system.
pub fn setup(level_filter: Option<log::LevelFilter>, lib_filters: Vec<(&'static str, log::LevelFilter)>) {
    use fern::colors::{Color, ColoredLevelConfig};
    let colors = ColoredLevelConfig::default()
        .info(Color::Green)
        .debug(Color::BrightMagenta)
        .trace(Color::BrightBlue);
    // This sets up a `fern` logger and initializes `log`.
    let mut logger_setup = fern::Dispatch::new()
        // Formats logs
        .format(move |out, message, record| {
            out.finish(format_args!(
                "{:<5} |{:<8} \x1b[38;5;240m{}\x1b[0m {}|\t {}{}{} {}[@{}]\x1b[0m",
                colors.color(record.level()),
                thread::current().name().unwrap_or("?"),
                process::id(),
                chrono::Local::now().format("%Y-%m-%d %H:%M:%S"),
                if record.level() == Level::Error {"\x1b[38;5;9m"} else {""},
                message,
                if record.level() == Level::Error {"\x1b[0m"} else {""},
                if record.level() == Level::Error {"\x1b[38;5;1m"} else {"\x1b[38;5;240m"},
                record.target()
            ))
        })
        .level(level_filter.unwrap_or(log::LevelFilter::Trace));
        // Filter out unnecessary stuff
    for (lib_name, level) in lib_filters {
        logger_setup = logger_setup.level_for(lib_name, level);
    }
    logger_setup
        // Hooks up console output.
        // env var for outputting to a file?
        // Haven't needed it yet!
        .chain(std::io::stdout())
        .apply()
        .expect("Could not init logging!");
}


/// Color coding, with a fast acceleration. Min is the normal state, and the higher, the worse.
pub fn scale_bad(string: &str, min: f64, max: f64, value: f64, /*importance: f64/*, neutral_start: bool*/*/) -> String {
    let value = value.min(max); //[feat] mention when above max
    let normed = ((value-min)/(max-min));
    let hue = 60. - (normed*60.);
    let sat = ((normed*7.).tanh());
    let val = ((normed*3.).tanh()/2.)+0.5;
    let green: Hsv<_, f64> = palette::Hsv::new(hue, sat, val);

    let color: LinSrgb<f64> = green.into();
    //format!("{} {} {}  ", (color.red*255.).round() as u8, (color.green*255.).round() as u8, (color.blue*255.).round() as u8)
    format!("\x1b[38;2;{};{};{}m{}\x1b[38;2;0m", (color.red*255.).round() as u8, (color.green*255.).round() as u8, (color.blue*255.).round() as u8, string)
}

