use log::{warn, error};
use std::format_args;

/// A non panicking alternative to todo! for when it makes sense. Logs a warning.
#[macro_export]
macro_rules! todo_warn {
    () => {
        use log::warn;//[todo][ignore_me] any alternative to this?
        (warn!("not yet implemented"))
    };
    ($($arg:tt)+) => {
        use log::warn;//[todo][ignore_me] any alternative to this?
        (warn!("not yet implemented: {}", format_args!($($arg)+)))
    };
}

/// A non panicking alternative to todo! for when it makes sense. Logs an error.
#[macro_export]
macro_rules! todo_error {
    () => {
        use log::error;//[todo][ignore_me] any alternative to this?
        (error!("not yet implemented"))
    };
    ($($arg:tt)+) => {
        use log::error;//[todo][ignore_me] any alternative to this?
        (error!("not yet implemented: {}", format_args!($($arg)+)))
    };
}

/// Formats an info log to pop up a bit. For quick checks.
#[macro_export]
macro_rules! say {
    ($($arg:tt)+) => {
        {use log::info;//[todo][ignore_me] any alternative to this?
        info!("\x1b[38;2;255;255;100m    {}\x1b[0m", format_args!($($arg)+))}
    };
}

/// Formats an info log to pop up a bit more. For quick checks.
#[macro_export]
macro_rules! shout {
    ($($arg:tt)+) => {
        {use log::info;//[todo][ignore_me] any alternative to this?
        info!("\x1b[38;2;255;100;100m    {}\x1b[0m", format_args!($($arg)+))}
    };
}
