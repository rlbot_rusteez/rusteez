use rlbot::Vector3;

#[derive(Clone, PartialEq, Debug)]
pub struct Position {
    pub location: PhysicalState,
    pub rotation: PhysicalState
}

#[derive(Clone, PartialEq, Debug)]
pub struct PhysicalState {
    pub position: [f64;3],
    pub velocity: [f64;3],
    pub acceleration: [f64;3],
}



impl From<&Vector3> for Position {
    fn from(vector: &Vector3) -> Self {
        Position {
            location: PhysicalState {
                position: [vector.x as f64, vector.y as f64, vector.z as f64],
                velocity: [0.0;3],
                acceleration: [0.0;3]
            },
            rotation: PhysicalState {
                position: [0.0;3],
                velocity: [0.0;3],
                acceleration: [0.0;3]
            }
        }
    }
}

impl Default for Position {
    fn default() -> Self {
        Position {
            location: PhysicalState {
                position: [0.0;3],
                velocity: [0.0;3],
                acceleration: [0.0;3]
            },
            rotation: PhysicalState {
                position: [0.0;3],
                velocity: [0.0;3],
                acceleration: [0.0;3]
            }
        }
    }
}